namespace Roguelike.Components
{
    public class ActionComponent : Component
    {
        public ActionType ActionType { get; }
        public Entity Target { get; }

        public ActionComponent(ActionType actionType, Entity target) : base(ComponentLookup.Action)
        {
            ActionType = actionType;
            Target = target;
        }
    }

    public enum ActionType
    {
        PickUp,
        Drop,
        Interact,
        EquipArmor,
        EquipWeapon,
        UnequipArmor,
        UnequipWeapon
    }
}