namespace Roguelike.Components
{
    public class ArmorComponent : Component
    {
        public int ArmorClass { get; }
        
        public ArmorComponent(int armorClass) : base (ComponentLookup.Armor)
        {
            ArmorClass = armorClass;
        }
    }
}