using System;

namespace Roguelike.Components
{
    public class Component
    {
        public Entity RetainedBy { get; private set; }

        public readonly ComponentLookup Type;

        public Component(ComponentLookup type)
        {
            Type = type;
        }

        public void Retain(Entity entity)
        {
            if (RetainedBy != null) throw new Exception($"Something is really wrong! Component is already retained by {RetainedBy.Name}");
            RetainedBy = entity;
        }
    }

    public enum ComponentLookup
    {
        Action,
        Armor,
        BlockMovement,
        BlockVision,
        Draw,
        Equipment,
        EquipmentSlot,
        Health,
        Inventory,
        Item,
        Movement,
        Position,
        UniqueLookup,
        Viewer,
        Weapon,
        HighLoad,
        Dead,
        DropOnDeath,
    }
}