namespace Roguelike.Components
{
    public class DeadComponent : Component
    {
        public DeadComponent() : base(ComponentLookup.Dead)
        {
            
        }
    }
}