namespace Roguelike.Components.Debug
{
    public class HighLoadComponent : Component
    {
        public string HighLoadString;

        public HighLoadComponent() : base (ComponentLookup.HighLoad)
        {
            HighLoadString = "I am gonna destroy your CPU!!!";
        }
    }
}