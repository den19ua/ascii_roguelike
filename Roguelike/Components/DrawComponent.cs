using System.Collections.Generic;
using OpenTK.Graphics;

namespace Roguelike.Components
{
    public class DrawComponent : Component
    {
        public char Char { get; }
        public Color4 Color { get; }
        public DrawLayer Layer { get; }
        
        public DrawComponent(char c, Color4 color, DrawLayer layer) : base(ComponentLookup.Draw)
        {
            Char = c;
            Color = color;
            Layer = layer;
        }
    }

    public enum DrawLayer
    {
        Tiles = 0,
        Items = 1,
        Creatures = 2
    }
    
    public class ByDrawLayer : IComparer<Entity>
    {
        private DrawComponent _xDraw;
        private DrawComponent _yDraw;
        
        
        public int Compare(Entity x, Entity y)
        {
            if (x == null || y == null) return 1;
            if (x == y)
            {
                return 0;
            }

            _xDraw = x.GetComponent<DrawComponent>(ComponentLookup.Draw);
            _yDraw = y.GetComponent<DrawComponent>(ComponentLookup.Draw);
            if (_xDraw == null || _yDraw == null || _xDraw.Layer == _yDraw.Layer)
            {
                return x.Id.CompareTo(y.Id);
            }

            if (_xDraw.Layer > _yDraw.Layer)
            {
                return 1;
            }

            return -1;
        }
    }
}