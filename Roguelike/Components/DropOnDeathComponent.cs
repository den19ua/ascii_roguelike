namespace Roguelike.Components
{
    public class DropOnDeathComponent : Component
    {
        public Entity Drop { get; }
        
        public DropOnDeathComponent(Entity drop) : base(ComponentLookup.DropOnDeath)
        {
            Drop = drop;
        }
    }
}