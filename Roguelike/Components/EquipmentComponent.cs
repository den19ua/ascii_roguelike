namespace Roguelike.Components
{
    public class EquipmentComponent : Component
    {
        public EquipmentSlotType SlotType { get; }

        public EquipmentComponent(EquipmentSlotType slotType) : base (ComponentLookup.Equipment)
        {
            SlotType = slotType;
        }
    }
}