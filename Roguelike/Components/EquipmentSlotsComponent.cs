
using System.Collections.Generic;

namespace Roguelike.Components
{
    public class EquipmentSlotsComponent : Component
    {
        public List<EquipmentSlot> EquipmentSlots { get; }
        
        public EquipmentSlotsComponent() : base (ComponentLookup.EquipmentSlot)
        {
            EquipmentSlots = new List<EquipmentSlot>();
            var headSlot = new EquipmentSlot(EquipmentSlotType.Head);
            var chestSlot = new EquipmentSlot(EquipmentSlotType.Chest);
            var legsSlot = new EquipmentSlot(EquipmentSlotType.Legs);
            var weaponSlot = new EquipmentSlot(EquipmentSlotType.Weapon);
            
            EquipmentSlots.Add(headSlot);
            EquipmentSlots.Add(chestSlot);
            EquipmentSlots.Add(legsSlot);
            EquipmentSlots.Add(weaponSlot);
        }
    }

    public class EquipmentSlot
    {
        public EquipmentComponent Equipment { get; set; }
        public EquipmentSlotType SlotType { get; }

        public EquipmentSlot(EquipmentSlotType slotType)
        {
            SlotType = slotType;
        }
    }
    
    public enum EquipmentSlotType
    {
        Weapon,
        Head,
        Chest,
        Legs
    }
}