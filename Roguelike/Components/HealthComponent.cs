namespace Roguelike.Components
{
    public class HealthComponent : Component
    {
        public int MaxHealth { get; }
        public int Health { get; }

        public HealthComponent(int maxHealth, int health) : base (ComponentLookup.Health)
        {
            MaxHealth = maxHealth;
            Health = health;
        }
    }
}