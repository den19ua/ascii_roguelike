using System.Collections.Generic;

namespace Roguelike.Components
{
    public class InventoryComponent : Component
    {
        public List<ItemComponent> Items { get; }
        public bool WaitingForPickUp { get; set; }
        
        public InventoryComponent() : base (ComponentLookup.Inventory)
        {
            Items = new List<ItemComponent>();
        }

        //void PickUp(Item item);
        //Item DropItem();
    }
}