namespace Roguelike.Components
{
    public class MovementComponent : Component
    {
        public Vector2 Movement { get; }

        public MovementComponent(Vector2 movement) : base (ComponentLookup.Movement)
        {
            Movement = movement;
        }
    }
}