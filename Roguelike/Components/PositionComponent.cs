namespace Roguelike.Components
{
    public class PositionComponent : Component
    {
        public Vector2 Position { get; }

        public PositionComponent(Vector2 position) : base (ComponentLookup.Position)
        {
            Position = position;
        }
    }
}