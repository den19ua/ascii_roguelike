namespace Roguelike.Components
{
    public class UniqueLookupComponent : Component
    {
        public string UniqueLookup { get; }

        public UniqueLookupComponent(string uniqueLookup) : base (ComponentLookup.UniqueLookup)
        {
            UniqueLookup = uniqueLookup;
        }
    }
}