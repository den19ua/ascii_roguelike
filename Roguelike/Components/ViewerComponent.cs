using System.Collections.Generic;

namespace Roguelike.Components
{
    public class ViewerComponent : Component
    {
        public List<Vector2> VisibleTiles { get; }
        //public bool[,] KnownTiles { get; }
        public HashSet<Vector2> KnownTiles { get; }
        public int ViewDistance { get; }

        public ViewerComponent(Vector2 worldSize, int viewDistance) : base (ComponentLookup.Viewer)
        {
            ViewDistance = viewDistance;
            VisibleTiles = new List<Vector2>();
            KnownTiles = new HashSet<Vector2>();
            //KnownTiles = new bool[worldSize.X, worldSize.Y];
        }
    }
}