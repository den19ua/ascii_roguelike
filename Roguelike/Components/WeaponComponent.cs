namespace Roguelike.Components
{
    public class WeaponComponent : Component
    {
        public int Damage { get; }
        
        public WeaponComponent(int damage) : base (ComponentLookup.Weapon)
        {
            Damage = damage;
        }
    }
}