using System;
using System.Collections.Generic;
using System.Linq;
using Roguelike.Components;

namespace Roguelike
{
    public class Entity : IComparable
    {
        private readonly GameWorld _gameWorld;
        public string Name { get; }
        public int Id { get; }

        private readonly Component[] _components = new Component[100];

        public Entity(string name, GameWorld gameWorld)
        {
            _gameWorld = gameWorld;
            Name = name;
            Id = gameWorld.GetEntityId();
        }

        public void AddComponent(Component component, bool silently = false)
        {
            if (_components[(int)component.Type] != null)
                throw new Exception($"Entity already has {component.GetType()} component!");
            
            _components[(int) component.Type] = component;
            component.Retain(this);
            if (!silently)
                _gameWorld.OnEntityComponentAdded(this, component);
        }

        public void RemoveComponent(ComponentLookup componentType, bool silently = false)
        {
            var component = _components[(int)componentType];
            RemoveComponent(component, silently);
        }

        public void ReplaceComponent<T>(T component, bool silently = false) where T : Component
        {
            var oldComponentType = component.Type;
            var oldComponent = GetComponent<T>(oldComponentType);
            if (oldComponent != null)
                RemoveComponent(oldComponentType, true);
            
            AddComponent(component, true);
            if (!silently)
                _gameWorld.OnEntityComponentChanged(this, component, oldComponent);
        }

        /// <summary>
        /// If you have ComponentLookup collection - better call HasComponent in foreach,
        /// so you don't waste time doing .ToArray
        /// </summary>
        public bool HasComponents(params ComponentLookup[] componentTypes)
        {
            return componentTypes.All(HasComponent);
        }

        public bool HasComponent(ComponentLookup componentType)
        {
            return _components[(int) componentType] != null;
        }

        public T GetComponent<T>(ComponentLookup type) where T : Component
        {
            return _components[(int)type] as T;
        }

        public void Destroy()
        {
            var components = new List<Component>();
            components.AddRange(_components);
            _gameWorld.OnEntityDestroyed(this);
            foreach (var component in components)
            {
                RemoveComponent(component);
            }
        }

        public static bool operator == (Entity entity1, Entity entity2)
        {
            return entity1?.Id == entity2?.Id;
        }

        public static bool operator !=(Entity entity1, Entity entity2)
        {
            return !(entity1 == entity2);
        }

        public override bool Equals(object obj)
        {
            return obj is Entity entity && entity.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public int CompareTo(object obj)
        {
            if (obj is Entity entity && entity.Id == Id) return 0;
            return 1;
        }

        private void RemoveComponent(Component component, bool silently = false)
        {
            if (component == null || _components[(int)component.Type] == null) return;
            
            if (!silently)
                _gameWorld.OnEntityBeforeComponentRemoved(this, component);
            
            _components[(int)component.Type] = null;
            if (!silently)
                _gameWorld.OnEntityComponentRemoved(this, component);
            //Console.WriteLine($"{component.Type} removed");
        }
    }
}