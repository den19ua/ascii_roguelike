﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics;
using Roguelike.Components;
using Roguelike.Systems;
using Roguelike.Systems.Debug;
using SunshineConsole;

namespace Roguelike
{
    public class GameWorld
    {
        public Vector2 WorldSize { get; }
        public bool NextTurn { get; set; }

        private readonly List<ContinuousEntitySystem> _continuousSystems = new List<ContinuousEntitySystem>();
        private readonly List<ReactiveEntitySystem> _reactiveSystems = new List<ReactiveEntitySystem>();
        private readonly List<Entity>[,] _entityMask;
        private readonly Dictionary<string, Entity> _uniqueLookupEntities = new Dictionary<string, Entity>();
        
        public event Action<Entity, Component> EntityComponentAdded = delegate { };
        public event Action<Entity, Component> EntityComponentRemoved = delegate { };
        public event Action<Entity, Component> EntityBeforeComponentRemoved = delegate { };
        public event Action<Entity, Component, Component> EntityComponentChanged = delegate { };
        
        public event Action<Entity> EntityDestroyed = delegate { };


        public GameWorld(Vector2 worldSize, ConsoleWindow consoleWindow)
        {
            WorldSize = worldSize;
            _entityMask = new List<Entity>[WorldSize.X, WorldSize.Y];

            AddSystem(new HighLoadSystem(this));
            
            AddSystem(new MoveSystem(this));
            AddSystem(new PositionMaskSystem(this));
            AddSystem(new MoveAttackSystem(this));
            AddSystem(new DeathSystem(this));
            AddSystem(new DropOnDeathSystem(this));

            AddSystem(new PickUpSystem(this));
            AddSystem(new ActionSystem(this));
            AddSystem(new RemoveDeadSystem(this));
            AddSystem(new ShadowCastSystem(this));
            AddSystem(new DrawSystem(this, consoleWindow));


            NextTurn = true;
        }

        public void Update()
        {
            //if (!NextTurn) return;
            foreach (var system in _continuousSystems)
            {
                system.Execute();
            }

            NextTurn = false;
        }

        private int _lastEntityId;
        public int GetEntityId()
        {
            var id = _lastEntityId;
            _lastEntityId++;
            return id;
        }

        public void DestroyEntity(Entity entity)
        {
            var uniqueLookup = entity.GetComponent<UniqueLookupComponent>(ComponentLookup.UniqueLookup);
            if (uniqueLookup != null)
                _uniqueLookupEntities.Remove(uniqueLookup.UniqueLookup);
            
            entity.Destroy();
        }

        public Entity GetEntityWithUniqueLookup(string lookup)
        {
            if (_uniqueLookupEntities.ContainsKey(lookup)) return _uniqueLookupEntities[lookup];
            
            Console.WriteLine($"Can't find entity with unique lookup {lookup}!");
            return null;
        }

        public IEnumerable<Entity> GetEntitiesAtPosition(Vector2 position)
        {
            var entities = _entityMask[position.X, position.Y];
            return entities ?? new List<Entity>();
        }

        public void ChangeEntityMask(Entity entity, Vector2 newPosition, Vector2 oldPosition)
        {
            RemoveFromEntityMask(entity, oldPosition);
            AddToEntityMask(entity, newPosition);
        }

        public void AddToEntityMask(Entity entity, Vector2 position)
        {
            if (_entityMask[position.X, position.Y] == null)
                _entityMask[position.X, position.Y] = new List<Entity>();
            
            _entityMask[position.X, position.Y].Add(entity);
        }
        
        public void RemoveFromEntityMask(Entity entity, Vector2 position)
        {
            if (_entityMask[position.X, position.Y] == null)
                return;
            
            _entityMask[position.X, position.Y].Remove(entity);
            Console.WriteLine($"Removed {entity.Name} from mask");
        }

        public bool IsInsideWorld(Vector2 position)
        {
            return position.X >= 0 && position.X < WorldSize.X && position.Y >= 0 && position.Y < WorldSize.Y;
        }
        
        private void AddSystem(ContinuousEntitySystem system)
        {
            if (_continuousSystems.Contains(system))
                throw new ArgumentException($"Trying to double-add system of type {system.GetType()}!");
            
            _continuousSystems.Add(system);
        }
        
        private void AddSystem(ReactiveEntitySystem system)
        {
            if (_reactiveSystems.Contains(system))
                throw new ArgumentException($"Trying to double-add system of type {system.GetType()}!");
            
            _reactiveSystems.Add(system);
        }

        public void OnEntityComponentAdded(Entity entity, Component component)
        {
            if (component is UniqueLookupComponent uniqueLookup)
                _uniqueLookupEntities.Add(uniqueLookup.UniqueLookup, entity);
            
            foreach (var system in _continuousSystems)
            {
                system.AddEntity(entity);
            }
            
            EntityComponentAdded(entity, component);
        }

        public void OnEntityComponentRemoved(Entity entity, Component component)
        {
            EntityComponentRemoved(entity, component);
        }

        public void OnEntityBeforeComponentRemoved(Entity entity, Component component)
        {
            EntityBeforeComponentRemoved(entity, component);
        }

        public void OnEntityComponentChanged(Entity entity, Component newComponent, Component oldComponent)
        {
            EntityComponentChanged(entity, newComponent, oldComponent);
        }

        public void OnEntityDestroyed(Entity entity)
        {
            EntityDestroyed(entity);
        }
    }
}