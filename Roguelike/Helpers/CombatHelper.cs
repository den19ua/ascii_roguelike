using System;
using System.Linq;
using Roguelike.Components;

namespace Roguelike.Helpers
{
    public static class CombatHelper
    {
        public static bool HasWeapon(Entity entity)
        {
            return GetWeapon(entity) != null;
        }

        public static WeaponComponent GetWeapon(Entity entity)
        {
            var equipmentSlotsComponent = entity.GetComponent<EquipmentSlotsComponent>(ComponentLookup.EquipmentSlot);
            var weaponSlot = equipmentSlotsComponent?.EquipmentSlots.FirstOrDefault(s => s.SlotType == EquipmentSlotType.Weapon);
            return weaponSlot?.Equipment?.RetainedBy.GetComponent<WeaponComponent>(ComponentLookup.Weapon);
        }

        public static void Attack(Entity target, Entity attacker)
        {
            var weapon = GetWeapon(attacker);
            if (weapon == null)
            {
                Console.WriteLine($"Can't attack: {attacker.Name} has no weapon.");
                return;
            }

            var health = target.GetComponent<HealthComponent>(ComponentLookup.Health);
            if (health == null)
            {
                Console.WriteLine($"Can't attack: {target.Name} has no health component.");
                return;
            }

            var newHealthValue = health.Health - weapon.Damage;
            //Console.WriteLine($"Attacking {target.Name} with {weapon.RetainedBy.Name}. {target.Name} now has {newHealthValue} HP.");
            target.ReplaceComponent(new HealthComponent(health.MaxHealth, newHealthValue));
        }
    }
}