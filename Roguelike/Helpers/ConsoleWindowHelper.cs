using OpenTK.Graphics;
using SunshineConsole;

namespace Roguelike.Helpers
{
    public static class ConsoleWindowHelper
    {
        public static void ClearConsole(ConsoleWindow consoleWindow)
        {
            for (int x = 0; x < consoleWindow.Width; x++)
            {
                for (int y = 0; y < consoleWindow.Height; y++)
                {
                    consoleWindow.Write(y, x, ' ', Color4.Black);
                }
            }
        }
    }
}