using System;
using System.Threading;
using Roguelike.Components;

namespace Roguelike.Helpers
{
    public static class EquipmentHelper
    {
        public static void Equip(Entity equipmentEntity, Entity targetEntity, bool fromInventory)
        {
            var equipmentComponent = equipmentEntity.GetComponent<EquipmentComponent>(ComponentLookup.Equipment);
            if (equipmentComponent == null)
                throw new ArgumentException($"{equipmentEntity.Name} is not an equipment!");
            
            var equipmentSlotsComponent = targetEntity.GetComponent<EquipmentSlotsComponent>(ComponentLookup.EquipmentSlot);
            if (equipmentSlotsComponent == null)
                throw new ArgumentException($"{targetEntity.Name} does not have equipment component!");

            var equipmentSlot = FindSuitableSlot(equipmentComponent, equipmentSlotsComponent);
            if (equipmentSlot == null)
                throw new ArgumentException($"Can't find armor slot for type {equipmentComponent.SlotType}.");
            
            if (equipmentSlot.Equipment != null)
                Unequip(equipmentSlot.Equipment.RetainedBy, targetEntity);

            equipmentSlot.Equipment = equipmentComponent;
            
            if (fromInventory)
                ItemHelper.RemoveItem(equipmentEntity, targetEntity);
            //Console.WriteLine($"Now wearing {equipmentEntity.Name}");
        }

        public static void Unequip(Entity equipmentEntity, Entity targetEntity)
        {
            var equipmentSlotsComponent = targetEntity.GetComponent<EquipmentSlotsComponent>(ComponentLookup.EquipmentSlot);
            if (equipmentSlotsComponent == null)
                throw new ArgumentException($"{targetEntity.Name} does not have equipment component!");

            var equipmentComponent = equipmentEntity.GetComponent<EquipmentComponent>(ComponentLookup.Equipment);
            if (equipmentComponent == null)
                throw new ArgumentException($"{equipmentEntity.Name} is not an equipment!");
            
            foreach (var equipmentSlot in equipmentSlotsComponent.EquipmentSlots)
            {
                if (equipmentSlot.Equipment != equipmentComponent) continue;
                
                equipmentSlot.Equipment = null;
                ItemHelper.AddItem(equipmentComponent.RetainedBy, targetEntity);
                return;
            }
            
            Console.WriteLine("Nothing to unequip!");
        }
        
        public static int GetArmorClass(Entity entity)
        {
            var equipmentSlotsComponent = entity.GetComponent<EquipmentSlotsComponent>(ComponentLookup.EquipmentSlot);
            if (equipmentSlotsComponent == null) return 0;

            var armorClass = 0;
            foreach (var equipmentSlot in equipmentSlotsComponent.EquipmentSlots)
            {
                var armorComponent = equipmentSlot?.Equipment?.RetainedBy.GetComponent<ArmorComponent>(ComponentLookup.Armor);
                if (armorComponent == null) continue;
                armorClass += armorComponent.ArmorClass;
            }

            return armorClass;
        }

        private static EquipmentSlot FindSuitableSlot(EquipmentComponent equipmentComponent, EquipmentSlotsComponent equipmentSlotsComponent)
        {
            foreach (var equipmentSlot in equipmentSlotsComponent.EquipmentSlots)
            {
                if (equipmentSlot.SlotType == equipmentComponent.SlotType) return equipmentSlot;
            }

            return null;
        }
    }
}