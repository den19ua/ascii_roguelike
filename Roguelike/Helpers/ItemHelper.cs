using System;
using Roguelike.Components;

namespace Roguelike.Helpers
{
    public static class ItemHelper
    {
        public static void PickUpItem(Entity itemEntity, Entity targetEntity)
        {
            AddItem(itemEntity, targetEntity);
            itemEntity.RemoveComponent(ComponentLookup.Position);
        }

        public static void DropItem(Entity itemEntity, Entity targetEntity)
        {
            RemoveItem(itemEntity, targetEntity);
            var position = targetEntity.GetComponent<PositionComponent>(ComponentLookup.Position).Position;
            DropItem(itemEntity, position);
        }

        public static void DropItem(Entity itemEntity, Vector2 position)
        {
            itemEntity.AddComponent(new PositionComponent(position));
        }
        
        public static void AddItem(Entity itemEntity, Entity targetEntity)
        {
            var item = itemEntity.GetComponent<ItemComponent>(ComponentLookup.Item);
            if (item == null)
            {
                Console.WriteLine($"{itemEntity.Name} is not an item!");
                return;
            }
            
            var inventory = targetEntity.GetComponent<InventoryComponent>(ComponentLookup.Inventory);
            if (inventory == null)
            {
                Console.WriteLine($"Can't add {itemEntity.Name}: {targetEntity.Name} has no inventory!");
                return;
            }
            
            inventory.Items.Add(item);
            Console.WriteLine($"Added {itemEntity.Name} to inventory.");
        }

        public static void RemoveItem(Entity itemEntity, Entity targetEntity)
        {
            var item = itemEntity.GetComponent<ItemComponent>(ComponentLookup.Item);
            if (item == null)
            {
                Console.WriteLine($"{itemEntity.Name} is not an item!");
                return;
            }
            
            var inventory = targetEntity.GetComponent<InventoryComponent>(ComponentLookup.Inventory);
            if (inventory == null)
            {
                Console.WriteLine($"Can't remove {itemEntity.Name}: {targetEntity.Name} has no inventory!");
                return;
            }

            if (!inventory.Items.Remove(item))
            {
                Console.WriteLine($"Can't remove {itemEntity.Name}: {targetEntity.Name} doesn't have it!");
                return;
            }
            
            Console.WriteLine($"Removed {itemEntity.Name} from inventory.");
        }
    }
}