using System;
using Roguelike.Components;

namespace Roguelike.Helpers
{
    public static class PositionHelper
    {
        public static void AddMovement(Vector2 movement, Entity entity)
        {
            var movementComponent = entity.GetComponent<MovementComponent>(ComponentLookup.Movement);
            if (movementComponent == null)
            {
                Console.WriteLine($"{entity.Name} does not have movement component!");
                return;
            }

            entity.ReplaceComponent(new MovementComponent(movementComponent.Movement + movement));
        }
    }
}