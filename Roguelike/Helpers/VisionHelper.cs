using System.Collections.Generic;
using Roguelike.Components;

namespace Roguelike.Helpers
{
    public static class VisionHelper
    {
        public static void AddToVision(Vector2 tile, Entity entity)
        {
            var viewerComponent = entity.GetComponent<ViewerComponent>(ComponentLookup.Viewer);
            if (viewerComponent == null) return;
            
            viewerComponent.VisibleTiles.Add(tile);
            viewerComponent.KnownTiles.Add(tile);
            //viewerComponent.KnownTiles[tile.X, tile.Y] = true;
        }
        
        public static void AddToVision(ICollection<Vector2> tiles, Entity entity)
        {
            var viewerComponent = entity.GetComponent<ViewerComponent>(ComponentLookup.Viewer);
            if (viewerComponent == null) return;
            
            foreach (var tile in tiles)
            {
                viewerComponent.VisibleTiles.Add(tile);
                viewerComponent.KnownTiles.Add(tile);
                //viewerComponent.KnownTiles[tile.X, tile.Y] = true;
            }
        }
    }
}