using System;
using OpenTK.Input;
using SunshineConsole;

namespace Roguelike
{
    public class InputManager
    {
        public InputState InputState { get; set; }
        
        private readonly ConsoleWindow _consoleWindow;

        public event Action<Vector2, InputState> MovePressed = delegate { };
        
        public event Action<InputState> EnterPressed = delegate { };
        
        public event Action PickUpPressed = delegate { };
        public event Action<InputState> DropPressed = delegate { };
        public event Action InventoryPressed = delegate { };
        public event Action EquipmentPressed = delegate { };
        
        //todo: remove
        public event Action UnequipPressed = delegate { };

        public InputManager(ConsoleWindow consoleWindow)
        {
            _consoleWindow = consoleWindow;
        }
        
        public void Update()
        {
            if (!_consoleWindow.KeyPressed) return;
            
            var key = _consoleWindow.GetKey();
            switch (key)
            {
                case Key.Up:
                    MovePressed(Vector2.Up, InputState);
                    break;
                case Key.Down:
                    MovePressed(Vector2.Down, InputState);
                    break; 
                case Key.Left:
                    MovePressed(Vector2.Left, InputState);
                    break;
                case Key.Right:
                    MovePressed(Vector2.Right, InputState);
                    break;
                case Key.Enter:
                    EnterPressed(InputState);
                    break;
                case Key.G:
                    PickUpPressed();
                    break;
                case Key.D:
                    DropPressed(InputState);
                    break;
                case Key.I:
                    InventoryPressed();
                    break;
                case Key.E:
                    EquipmentPressed();
                    break;
            }
        }
    }
    
    public enum InputState
    {
        Game,
        Window,
        Inventory,
        Equipment,
    }
}