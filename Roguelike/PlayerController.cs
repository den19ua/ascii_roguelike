using Roguelike.Components;
using Roguelike.Helpers;

namespace Roguelike
{
    public class PlayerController
    {
        private readonly GameWorld _gameWorld;
        private readonly Entity _playerEntity;

        public PlayerController(GameWorld gameWorld, Entity playerEntity, InputManager inputManager)
        {
            inputManager.MovePressed += Move;
            
            //todo: rewrite!!!
            inputManager.PickUpPressed += delegate { _playerEntity.GetComponent<InventoryComponent>(ComponentLookup.Inventory).WaitingForPickUp = true; };

            _gameWorld = gameWorld;
            _playerEntity = playerEntity;
        }

        private void Move(Vector2 move, InputState inputState)
        {
            if (inputState != InputState.Game) return;
            
            PositionHelper.AddMovement(move, _playerEntity);
            _gameWorld.NextTurn = true;
        }
    }
}