﻿using System;
using System.Linq;
using OpenTK.Graphics;
using Roguelike.Components;
using Roguelike.Components.Debug;
using Roguelike.Helpers;
using Roguelike.UI;
using Roguelike.UI.Windows;
using SunshineConsole;

namespace Roguelike
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var consoleWindow = new ConsoleWindow(40, 125, "Roguelike");
            var worldSize = new Vector2(20, 20);
            var gameWorld = new GameWorld(worldSize, consoleWindow);

            //SpawnPerformance(100000, gameWorld);
            TestFillMap(gameWorld);
            //TestSpawnHighLoadDragons(gameWorld);
            TestSpawnItems(gameWorld);

            var playerEntity = TestSpawnPlayer(gameWorld);

            var inputManager = new InputManager(consoleWindow);
            var playerController = new PlayerController(gameWorld, playerEntity, inputManager);
            var inventoryWindow = new InventoryWindow(playerEntity, "Inventory", consoleWindow, inputManager, InputState.Inventory);
            var equipmentWindow = new EquipmentWindow(playerEntity, "Equipment", consoleWindow, inputManager, InputState.Equipment);

            var hud = new HUD(playerEntity, consoleWindow);

            while (!consoleWindow.KeyPressed && consoleWindow.WindowUpdate())
            {
                inputManager.Update();
                gameWorld.Update();
                
                inventoryWindow.Draw();
                equipmentWindow.Draw();
                hud.Draw();
                /* WindowUpdate() does a few very important things:
                ** It renders the console to the screen;
                ** It checks for input events from the OS, such as keypresses, so that they
                **   can reach the program;
                ** It returns false if the console has been closed, so that the program
                **   can be properly ended. */
            }
        }

        private static void TestFillMap(GameWorld gameWorld)
        {
            int wallCount = 0;
            int floorCount = 0;
            for (int x = 0; x < gameWorld.WorldSize.X; x++)
            {
                for (int y = 0; y < gameWorld.WorldSize.Y; y++)
                {
                    var position = new PositionComponent(new Vector2(x, y));

                    if (x == 0 || y == 0 || x == gameWorld.WorldSize.X - 1 || y == gameWorld.WorldSize.Y - 1)
                    {
                        var wall = new Entity("Wall", gameWorld);
                        wall.AddComponent(position);
                        wall.AddComponent(new DrawComponent('#', Color4.DarkGray, DrawLayer.Tiles));
                        wall.AddComponent(new BlockMovementComponent());
                        wall.AddComponent(new BlockVisionComponent());
                        
                        wallCount++;
                    }
                    else
                    {
                        var floor = new Entity("Floor", gameWorld);
                        floor.AddComponent(position);
                        floor.AddComponent(new DrawComponent('.', Color4.DarkGray, DrawLayer.Tiles));

                        floorCount++;
                    }
                }
            }
            
            Console.WriteLine($"{wallCount} wall tiles spawned.");
            Console.WriteLine($"{floorCount} floor tiles spawned.");
        }

        private static void TestSpawnHighLoadDragons(GameWorld gameWorld)
        {
            int dragonsAmount = 0;
            for (int x = 0; x < gameWorld.WorldSize.X; x++)
            {
                for (int y = 0; y < gameWorld.WorldSize.Y; y++)
                {
                    if (x % 3 != 0 && y % 3 != 0) continue;
                    
                    var position = new Vector2(x, y);
                    if (gameWorld.GetEntitiesAtPosition(position).Any(e => e.HasComponent(ComponentLookup.BlockMovement))) continue;
                    
                    var dragon = new Entity("Dragon", gameWorld);
                    dragon.AddComponent(new PositionComponent(position));
                    dragon.AddComponent(new DrawComponent('D', Color4.Red, DrawLayer.Creatures));
                    dragon.AddComponent(new HealthComponent(10, 10));
                    dragon.AddComponent(new HighLoadComponent());
                    dragon.AddComponent(new EquipmentSlotsComponent());
                    
                    
                    var claws = new Entity("Claws", gameWorld);
                    claws.AddComponent(new WeaponComponent(1));
                    claws.AddComponent(new EquipmentComponent(EquipmentSlotType.Weapon));
                    EquipmentHelper.Equip(claws, dragon, false);
                    
                    var ring = new Entity("Ring", gameWorld);
                    ring.AddComponent(new DrawComponent('o', Color4.Gold, DrawLayer.Items));
                    ring.AddComponent(new ItemComponent());
                    dragon.AddComponent(new DropOnDeathComponent(ring));

                    dragonsAmount++;
                }
            }

            Console.WriteLine($"{dragonsAmount} dragons spawned.");
        }

        private static void TestSpawnItems(GameWorld gameWorld)
        {
            var ring = new Entity("Ring", gameWorld);
            ring.AddComponent(new PositionComponent(new Vector2(3, 4)));
            ring.AddComponent(new DrawComponent('o', Color4.Gold, DrawLayer.Items));
            ring.AddComponent(new ItemComponent());
            
            var amulet = new Entity("Amulet", gameWorld);
            amulet.AddComponent(new PositionComponent(new Vector2(6, 8)));
            amulet.AddComponent(new DrawComponent('a', Color4.Silver, DrawLayer.Items));
            amulet.AddComponent(new ItemComponent());
            
            var sword = new Entity("Sword", gameWorld);
            sword.AddComponent(new PositionComponent(new Vector2(7, 7)));
            sword.AddComponent(new DrawComponent('/', Color4.Silver, DrawLayer.Items));
            sword.AddComponent(new WeaponComponent(1));
            sword.AddComponent(new ItemComponent());
            sword.AddComponent(new EquipmentComponent(EquipmentSlotType.Weapon));
            
            var ironHelmet = new Entity("Iron helmet", gameWorld);
            ironHelmet.AddComponent(new PositionComponent(new Vector2(6, 7)));
            ironHelmet.AddComponent(new DrawComponent('n', Color4.DarkGray, DrawLayer.Items));
            ironHelmet.AddComponent(new ArmorComponent(1));
            ironHelmet.AddComponent(new ItemComponent());
            ironHelmet.AddComponent(new EquipmentComponent(EquipmentSlotType.Head));
            
            var steelHelmet = new Entity("Steel helmet", gameWorld);
            steelHelmet.AddComponent(new PositionComponent(new Vector2(6, 6)));
            steelHelmet.AddComponent(new DrawComponent('n', Color4.Silver, DrawLayer.Items));
            steelHelmet.AddComponent(new ArmorComponent(2));
            steelHelmet.AddComponent(new ItemComponent());
            steelHelmet.AddComponent(new EquipmentComponent(EquipmentSlotType.Head));
            
            var steelBreastplate = new Entity("Steel breastplate", gameWorld);
            steelBreastplate.AddComponent(new PositionComponent(new Vector2(5, 6)));
            steelBreastplate.AddComponent(new DrawComponent('B', Color4.Silver, DrawLayer.Items));
            steelBreastplate.AddComponent(new ArmorComponent(6));
            steelBreastplate.AddComponent(new ItemComponent());
            steelBreastplate.AddComponent(new EquipmentComponent(EquipmentSlotType.Chest));
        }

        private static Entity TestSpawnPlayer(GameWorld gameWorld)
        {
            var player = new Entity("Player", gameWorld);
            player.AddComponent(new PositionComponent(new Vector2(5, 5)));
            player.AddComponent(new DrawComponent('@', Color4.Lime, DrawLayer.Creatures));
            //player.AddComponent(new HealthComponent(10, 10));
            player.AddComponent(new MovementComponent(Vector2.Zero));
            player.AddComponent(new InventoryComponent());
            player.AddComponent(new EquipmentSlotsComponent());
            player.AddComponent(new ViewerComponent(gameWorld.WorldSize, 7));
            player.AddComponent(new UniqueLookupComponent("Player"));

            return player;
        }
    }
}