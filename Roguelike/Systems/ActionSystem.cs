﻿using System.Collections.Generic;
using Roguelike.Components;
using Roguelike.Helpers;

namespace Roguelike.Systems
{
    public class ActionSystem : ContinuousEntitySystem
    {
        public ActionSystem(GameWorld gameWorld) : base(gameWorld)
        {
        }
        
        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Action);
        }

        protected override bool Filter(Entity entity)
        {
            return true;
        }

        protected override void Execute(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
            {
                var action = entity.GetComponent<ActionComponent>(ComponentLookup.Action);
                switch (action.ActionType)
                {
                    case ActionType.EquipWeapon:
                        EquipmentHelper.Equip(action.Target, entity, true);
                        break;
                    case ActionType.EquipArmor:
                        EquipmentHelper.Equip(action.Target, entity, true);
                        break;
                    case ActionType.UnequipWeapon:
                        EquipmentHelper.Unequip(action.Target, entity);
                        break;
                    case ActionType.UnequipArmor:
                        EquipmentHelper.Unequip(action.Target, entity);
                        break;
                }
                
                entity.RemoveComponent(ComponentLookup.Action);
            }
        }
    }
}