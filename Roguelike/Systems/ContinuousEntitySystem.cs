using System.Collections.Generic;
using System.Linq;

namespace Roguelike.Systems
{
    
    public abstract class ContinuousEntitySystem : EntitySystem
    {
        protected ContinuousEntitySystem(GameWorld gameWorld) : base (gameWorld)
        {

        }

        public override bool AddEntity(Entity entity)
        {
            return DoesEntityMatch(entity) && base.AddEntity(entity);
        }

        public void Execute()
        {
            if (!Entities.Any()) return;
            
            var filteredEntities = Entities.Where(Filter).ToList();
            if (!filteredEntities.Any()) return;
            
            Execute(filteredEntities);
        }

        protected abstract void Execute(IEnumerable<Entity> entities);
    }
}