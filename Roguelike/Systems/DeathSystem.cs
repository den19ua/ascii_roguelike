﻿using System;
using Roguelike.Components;

namespace Roguelike.Systems
{
    public class DeathSystem : ReactiveEntitySystem
    {
        public DeathSystem(GameWorld gameWorld) : base(gameWorld)
        {
        }
        
        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Health);
        }

        protected override bool Filter(Entity entity)
        {
            return entity.GetComponent<HealthComponent>(ComponentLookup.Health).Health <= 0;
        }
        
        protected override void SetTrigger()
        {
            SetTrigger(ComponentLookup.Health);
        }

        protected override void OnComponentChanged(Entity entity, Component newComponent, Component oldComponent)
        {
            entity.AddComponent(new DeadComponent());
            //GameWorld.DestroyEntity(entity);
            Console.WriteLine($"{entity.Name} died!");
        }
    }
}