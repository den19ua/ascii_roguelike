using System;
using System.Collections.Generic;
using Roguelike.Components;

namespace Roguelike.Systems.Debug
{
    public class HighLoadSystem : ContinuousEntitySystem
    {
        public HighLoadSystem(GameWorld gameWorld) : base(gameWorld)
        {

        }

        protected override bool Filter(Entity entity)
        {
            return true;
        }

        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.HighLoad);
        }

        protected override void Execute(IEnumerable<Entity> entities)
        {
            var random = new Random();
            
            foreach (var entity in entities)
            {
                entity.ReplaceComponent(new MovementComponent(new Vector2(random.Next(-1, 2), random.Next(-1, 2))));
            }
        }
    }
}