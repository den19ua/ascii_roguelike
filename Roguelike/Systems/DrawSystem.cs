using System.Collections.Generic;
using OpenTK.Graphics;
using Roguelike.Components;
using SunshineConsole;

namespace Roguelike.Systems
{
    public class DrawSystem : ContinuousEntitySystem
    {
        private readonly ConsoleWindow _consoleWindow;
        
        private readonly int _drawXSize;
        private readonly int _drawYSize;

        public DrawSystem(GameWorld gameWorld, ConsoleWindow consoleWindow) : base(gameWorld)
        {
            _consoleWindow = consoleWindow;
            _drawXSize = _consoleWindow.Cols;
            _drawYSize = _consoleWindow.Rows / 2;
        }

        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Position, ComponentLookup.Viewer);
        }

        protected override bool Filter(Entity entity)
        {
            return true;
        }

        protected override void CreateEntitiesSet()
        {
            Entities = new SortedSet<Entity>(new ByDrawLayer());
        }

        private void ClearMap()
        {
            for (var x = 0; x <= _drawXSize; x++)
            {
                for (var y = 0; y <= _drawYSize; y++)
                {
                    _consoleWindow.Write(y, x, ' ', Color4.Black);
                }
            }
        }

        protected override void Execute(IEnumerable<Entity> entities)
        {
            ClearMap();
            var sortedSet = new SortedDictionary<Entity, bool>(new ByDrawLayer());
            foreach (var entity in entities)
            {
                var viewerComponent = entity.GetComponent<ViewerComponent>(ComponentLookup.Viewer);
                var positionComponent = entity.GetComponent<PositionComponent>(ComponentLookup.Position);

                foreach (var position in viewerComponent.KnownTiles)
                {
                    foreach (var entityAtPosition in GameWorld.GetEntitiesAtPosition(position))
                    {
                        if (entityAtPosition.HasComponent(ComponentLookup.Draw))
                            sortedSet.Add(entityAtPosition, false);
                    }
                }
                
                foreach (var position in viewerComponent.VisibleTiles)
                {
                    foreach (var entityAtPosition in GameWorld.GetEntitiesAtPosition(position))
                    {
                        if (sortedSet.ContainsKey(entityAtPosition))
                            sortedSet[entityAtPosition] = true;
                        
                        else if (entityAtPosition.HasComponent(ComponentLookup.Draw))
                            sortedSet.Add(entityAtPosition, true);
                    }
                }

                foreach (var tileIsVisiblePair in sortedSet)
                {
                    var objectPositionComponent = tileIsVisiblePair.Key.GetComponent<PositionComponent>(ComponentLookup.Position);
                    var objectDrawComponent = tileIsVisiblePair.Key.GetComponent<DrawComponent>(ComponentLookup.Draw);
                    
                    var color = tileIsVisiblePair.Value ? objectDrawComponent.Color : Color4.Gray;
                    
                    var tilePosition = new Vector2(objectPositionComponent.Position.X - positionComponent.Position.X + _drawXSize / 2,
                        objectPositionComponent.Position.Y - positionComponent.Position.Y + _drawYSize / 2);
                    
                    if (tilePosition.Y < 0 || tilePosition.Y > _drawYSize) continue;
                    if (tilePosition.X < 0 || tilePosition.X > _drawXSize) continue;
                    
                    _consoleWindow.Write(tilePosition.Y, tilePosition.X, objectDrawComponent.Char, color);
                }
            }
        }
    }
}