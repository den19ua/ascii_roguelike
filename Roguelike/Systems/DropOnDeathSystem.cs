using Roguelike.Components;
using Roguelike.Helpers;

namespace Roguelike.Systems
{
    public class DropOnDeathSystem : ReactiveEntitySystem
    {
        public DropOnDeathSystem(GameWorld gameWorld) : base(gameWorld)
        {
            
        }
        
        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Dead, ComponentLookup.DropOnDeath, ComponentLookup.Position);
        }

        protected override bool Filter(Entity entity)
        {
            return true;
        }
        
        protected override void SetTrigger()
        {
            SetTrigger(ComponentLookup.Dead);
        }

        protected override void OnComponentAdded(Entity entity, Component component)
        {
            var positionComponent = entity.GetComponent<PositionComponent>(ComponentLookup.Position);
            var dropOnDeathComponent = entity.GetComponent<DropOnDeathComponent>(ComponentLookup.DropOnDeath);
            
            GameWorld.DestroyEntity(entity);
            ItemHelper.DropItem(dropOnDeathComponent.Drop, positionComponent.Position);
        }
    }
}