using System.Collections.Generic;
using Roguelike.Components;

namespace Roguelike.Systems
{
    public abstract class EntitySystem
    {
        protected GameWorld GameWorld { get; }
        protected ISet<Entity> Entities { get; set; }

        private readonly List<ComponentLookup> _matcher = new List<ComponentLookup>();

        public bool DoesEntityMatch(Entity entity)
        {
            foreach (var componentLookup in _matcher)
            {
                if (!entity.HasComponent(componentLookup)) return false;
            }

            return true;
        }

        public virtual bool AddEntity(Entity entity)
        {
            return Entities.Add(entity);
        }

        protected EntitySystem(GameWorld gameWorld)
        {
            gameWorld.EntityDestroyed += EntityOnDestroyed;
            gameWorld.EntityComponentRemoved += EntityOnComponentRemoved;
            
            GameWorld = gameWorld;
            InitSystem();
        }

        protected abstract bool Filter(Entity entity);

        protected abstract void SetMatcher();

        protected virtual void CreateEntitiesSet()
        {
            Entities = new HashSet<Entity>();
        }

        protected virtual void EntityOnDestroyed(Entity entity)
        {
            Entities.Remove(entity);
        }

        protected virtual void EntityOnComponentRemoved(Entity entity, Component component)
        {
            if (!_matcher.Contains(component.Type)) return;

            if (!DoesEntityMatch(entity))
                Entities.Remove(entity);
        }

        protected virtual void Init()
        {
            SetMatcher();
        }

        protected void SetMatcher(params ComponentLookup[] componentTypes)
        {
            _matcher.AddRange(componentTypes);
        }
        
        private void InitSystem()
        {
            CreateEntitiesSet();
            Init();
        }
        
    }
}