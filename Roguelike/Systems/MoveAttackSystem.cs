﻿using System.Linq;
using Roguelike.Components;
using Roguelike.Helpers;

namespace Roguelike.Systems
{
    public class MoveAttackSystem : ReactiveEntitySystem
    {
        public MoveAttackSystem(GameWorld gameWorld) : base(gameWorld)
        {
        }
        
        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Position, ComponentLookup.Movement,
                ComponentLookup.EquipmentSlot);
        }
        
        protected override void SetTrigger()
        {
            SetTrigger(ComponentLookup.Movement);
        }
        
        protected override bool Filter(Entity entity)
        {
            return entity.GetComponent<MovementComponent>(ComponentLookup.Movement).Movement != Vector2.Zero;
        }

        protected override void OnComponentChanged(Entity entity, Component newComponent, Component oldComponent)
        {
            if (!CombatHelper.HasWeapon(entity))
            {
                entity.ReplaceComponent(new MovementComponent(Vector2.Zero));
                return;
            }

            var position = entity.GetComponent<PositionComponent>(ComponentLookup.Position);
            var movement = entity.GetComponent<MovementComponent>(ComponentLookup.Movement);

            var desiredPosition = position.Position + movement.Movement;
            var entitiesAtPosition = GameWorld.GetEntitiesAtPosition(desiredPosition).ToArray();

            var otherEntity = entitiesAtPosition.FirstOrDefault(e => e.HasComponent(ComponentLookup.Health));
            if (otherEntity == null) return;

            CombatHelper.Attack(otherEntity, entity);
            entity.ReplaceComponent(new MovementComponent(Vector2.Zero));
        }
    }
}