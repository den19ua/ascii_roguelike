﻿using System;
using System.Linq;
using Roguelike.Components;

namespace Roguelike.Systems
{
    public class MoveSystem : ReactiveEntitySystem
    {
        public MoveSystem(GameWorld gameWorld) : base(gameWorld)
        {
        }

        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Position, ComponentLookup.Movement); //(typeof(PositionComponent), typeof(MovementComponent));
        }

        protected override void SetTrigger()
        {
            SetTrigger(ComponentLookup.Movement); //(typeof(MovementComponent));
        }

        protected override bool Filter(Entity entity)
        {
            return entity.GetComponent<MovementComponent>(ComponentLookup.Movement).Movement != Vector2.Zero;
        }

        protected override void OnComponentChanged(Entity entity, Component newComponent, Component oldComponent)
        {
            var position = entity.GetComponent<PositionComponent>(ComponentLookup.Position);
            var movement = entity.GetComponent<MovementComponent>(ComponentLookup.Movement);

            var desiredPosition = position.Position + movement.Movement;
            if (!GameWorld.IsInsideWorld(desiredPosition))
            {
                entity.ReplaceComponent(new MovementComponent(Vector2.Zero), true);
                return;
            }

            var entitiesAtPosition = GameWorld.GetEntitiesAtPosition(desiredPosition).ToArray();

            if (entitiesAtPosition.Any(e => e.HasComponent(ComponentLookup.Health)/*(typeof(HealthComponent))*/)) return;

            var notPassableTile = entitiesAtPosition.FirstOrDefault(e => e.HasComponent(ComponentLookup.BlockMovement)/*(typeof(BlockMovementComponent))*/);
            if (notPassableTile != null)
            {
                //Console.WriteLine($"Can't pass through {notPassableTile.Name} at {desiredPosition}");
                entity.ReplaceComponent(new MovementComponent(Vector2.Zero));
                return;
            }

            entity.ReplaceComponent(new PositionComponent(desiredPosition));
            entity.ReplaceComponent(new MovementComponent(Vector2.Zero), true);
        }
    }
}