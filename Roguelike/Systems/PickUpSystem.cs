﻿using System;
using System.Collections.Generic;
using System.Linq;
using Roguelike.Components;
using Roguelike.Helpers;

namespace Roguelike.Systems
{
    public class PickUpSystem : ContinuousEntitySystem
    {
        public PickUpSystem(GameWorld gameWorld) : base(gameWorld)
        {
        }
        
        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Position, ComponentLookup.Inventory); //(typeof(PositionComponent), typeof(InventoryComponent));
        }

        protected override bool Filter(Entity entity)
        {
            return entity.GetComponent<InventoryComponent>(ComponentLookup.Inventory).WaitingForPickUp;
        }

        protected override void Execute(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
            {
                var position = entity.GetComponent<PositionComponent>(ComponentLookup.Position);
                var inventory = entity.GetComponent<InventoryComponent>(ComponentLookup.Inventory);
                inventory.WaitingForPickUp = false;
                
                var itemsAtPosition = GameWorld.GetEntitiesAtPosition(position.Position).Where(e => e.HasComponent(ComponentLookup.Item)/*(typeof(ItemComponent))*/).ToList();

                foreach (var item in itemsAtPosition)
                    ItemHelper.PickUpItem(item, entity);

                inventory.WaitingForPickUp = false;
            }
        }
    }
}