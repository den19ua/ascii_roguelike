using Roguelike.Components;

namespace Roguelike.Systems
{
    public class PositionMaskSystem : ReactiveEntitySystem
    {
        public PositionMaskSystem(GameWorld gameWorld) : base(gameWorld)
        {
            
        }
        
        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Position); //(typeof(PositionComponent));
        }

        protected override void SetTrigger()
        {
            SetTrigger(ComponentLookup.Position); //(typeof(PositionComponent));
        }
        
        protected override bool Filter(Entity entity)
        {
            return true;
        }

        protected override void OnComponentChanged(Entity entity, Component newComponent, Component oldComponent)
        {
            if (!(oldComponent is PositionComponent oldPosition)) return;
            if (!(newComponent is PositionComponent newPosition)) return;
                
            GameWorld.ChangeEntityMask(entity, newPosition.Position, oldPosition.Position);
        }

        protected override void OnComponentAdded(Entity entity, Component component)
        {
            if (!(component is PositionComponent position)) return;
            //System.Console.WriteLine($"{entity.Name} added");
            GameWorld.AddToEntityMask(entity, position.Position);
        }

        protected override void OnBeforeComponentRemoved(Entity entity, Component component)
        {
            if (!(component is PositionComponent position)) return;
            
            GameWorld.RemoveFromEntityMask(entity, position.Position);
        }
    }
}