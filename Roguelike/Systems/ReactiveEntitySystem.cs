using System;
using System.Collections.Generic;
using System.Linq;
using Roguelike.Components;

namespace Roguelike.Systems
{
    public abstract class ReactiveEntitySystem : EntitySystem
    {
        //private readonly List<Type> _trigger = new List<Type>();
        private readonly List<ComponentLookup> _trigger = new List<ComponentLookup>();
        
        protected ReactiveEntitySystem(GameWorld gameWorld) : base(gameWorld)
        {
            GameWorld.EntityComponentChanged += EntityOnComponentChanged;
            GameWorld.EntityComponentAdded += EntityOnComponentAdded;
            GameWorld.EntityBeforeComponentRemoved += EntityOnBeforeComponentRemoved;
        }

        public override bool AddEntity(Entity entity)
        {
            if (!base.AddEntity(entity)) return false;
            
//            GameWorld.EntityComponentChanged += EntityOnComponentChanged;
//            GameWorld.EntityComponentAdded += EntityOnComponentAdded;
//            GameWorld.EntityBeforeComponentRemoved += EntityOnBeforeComponentRemoved;
            return true;
        }


        protected abstract void SetTrigger();

        protected virtual void OnComponentChanged(Entity entity, Component newComponent, Component oldComponent)
        {
            
        }

        protected virtual void OnComponentAdded(Entity entity, Component component)
        {
            
        }

        protected virtual void OnBeforeComponentRemoved(Entity entity, Component component)
        {
            
        }

        protected override void Init()
        {
            base.Init();
            SetTrigger();
        }

        protected override void EntityOnDestroyed(Entity entity)
        {
            base.EntityOnDestroyed(entity);
        }

        protected override void EntityOnComponentRemoved(Entity entity, Component component)
        {
            base.EntityOnComponentRemoved(entity, component);
        }

        protected void SetTrigger(params ComponentLookup[] componentTypes)
        {
            _trigger.AddRange(componentTypes);
        }

        private void EntityOnComponentChanged(Entity entity, Component newComponent, Component oldComponent)
        {
            if (!DoesEntityMatch(entity)) return;
            
            var componentType = newComponent.Type;

            var matchesAnyTrigger = false;
            foreach (var trigger in _trigger)
            {
                if (trigger == componentType)
                {
                    matchesAnyTrigger = true;
                    break;
                }
            }
            if (!matchesAnyTrigger) return;
            
            //if (!_trigger.Any(t => t == componentType || t.IsSubclassOf(componentType))) return;
            if (!Filter(entity)) return;
            
            OnComponentChanged(entity, newComponent, oldComponent);
        }
        
        private void EntityOnComponentAdded(Entity entity, Component component)
        {
            if (!DoesEntityMatch(entity)) return;
            
            var componentType = component.Type;
            var matchesAnyTrigger = false;
            foreach (var trigger in _trigger)
            {
                if (trigger != componentType) continue;
                
                matchesAnyTrigger = true;
                break;
            }
            if (!matchesAnyTrigger) return;
            
            //if (!_trigger.Any(t => t == componentType || t.IsSubclassOf(componentType))) return;
            if (!Filter(entity)) return;
            
            OnComponentAdded(entity, component);
        }
        
        private void EntityOnBeforeComponentRemoved(Entity entity, Component component)
        {
            if (!DoesEntityMatch(entity)) return;
            
            var componentType = component.Type;
            var matchesAnyTrigger = false;
            foreach (var trigger in _trigger)
            {
                if (trigger == componentType)
                {
                    matchesAnyTrigger = true;
                    break;
                }
            }
            if (!matchesAnyTrigger) return;
            
            //if (!_trigger.Any(t => t == componentType || t.IsSubclassOf(componentType))) return;
            if (!Filter(entity)) return;
            
            OnBeforeComponentRemoved(entity, component);
        }
    }
}