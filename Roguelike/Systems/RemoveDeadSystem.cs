using System.Collections.Generic;
using Roguelike.Components;

namespace Roguelike.Systems
{
    public class RemoveDeadSystem : ContinuousEntitySystem
    {
        public RemoveDeadSystem(GameWorld gameWorld) : base(gameWorld)
        {
        }
        
        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Dead);
        }

        protected override bool Filter(Entity entity)
        {
            return true;
        }

        protected override void Execute(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
            {
                GameWorld.DestroyEntity(entity);
            }
        }
    }
}