﻿using System;
using System.Collections.Generic;
using System.Linq;
using Roguelike.Components;
using Roguelike.Helpers;

namespace Roguelike.Systems
{
    public class ShadowCastSystem : ContinuousEntitySystem
    {
        public ShadowCastSystem(GameWorld gameWorld) : base(gameWorld)
        {
        }

        protected override void SetMatcher()
        {
            SetMatcher(ComponentLookup.Viewer, ComponentLookup.Position); //(typeof(ViewerComponent), typeof(PositionComponent));
        }

        protected override bool Filter(Entity entity)
        {
            return true;
        }

        protected override void Execute(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
            {
                var viewerComponent = entity.GetComponent<ViewerComponent>(ComponentLookup.Viewer);
                var position = entity.GetComponent<PositionComponent>(ComponentLookup.Position);
                
                viewerComponent.VisibleTiles.Clear();
                var visibleTiles = CalculateVisibility(GameWorld.WorldSize, position.Position, viewerComponent.ViewDistance);
                VisionHelper.AddToVision(visibleTiles, entity);
            }
        }

        private class OctantTransform
        {
            public int xx { get; }
            public int xy { get; }
            public int yx { get; }
            public int yy { get; }

            public OctantTransform(int xx, int xy, int yx, int yy)
            {
                this.xx = xx;
                this.xy = xy;
                this.yx = yx;
                this.yy = yy;
            }
        }

        private static readonly OctantTransform[] OctantTransforms =
        {
            new OctantTransform(1, 0, 0, 1), // 0 E-NE
            new OctantTransform(0, 1, 1, 0), // 1 NE-N
            new OctantTransform(0, -1, 1, 0), // 2 N-NW
            new OctantTransform(-1, 0, 0, 1), // 3 NW-W
            new OctantTransform(-1, 0, 0, -1), // 4 W-SW
            new OctantTransform(0, -1, -1, 0), // 5 SW-S
            new OctantTransform(0, 1, -1, 0), // 6 S-SE
            new OctantTransform(1, 0, 0, -1), // 7 SE-E
        };

        public List<Vector2> CalculateVisibility(Vector2 gridSize, Vector2 gridPosn, float viewRadius)
        {
            // Viewer's cell is always visible.
            //gridSize.SetLight(gridPosn.X, gridPosn.Y, 0.0f);
            var visibleTiles = new List<Vector2> {gridPosn};

            foreach (var octant in OctantTransforms)
            {
                CastLight(gridSize, gridPosn, viewRadius, 1, 1.0f, 0.0f, octant, ref visibleTiles);
            }

            return visibleTiles;
        }


        private void CastLight(Vector2 gridSize, Vector2 gridPos, float viewRadius,
            int startColumn, float leftViewSlope, float rightViewSlope, OctantTransform octantTransform, ref List<Vector2> visibleTiles)
        {
            var viewRadiusSqr = viewRadius * viewRadius;

            int viewCeiling = (int) Math.Ceiling(viewRadius);

            bool prevWasBlocked = false;

            float savedRightSlope = -1;

            for (int currentCol = startColumn; currentCol <= viewCeiling; currentCol++)
            {
                int xc = currentCol;

                for (int yc = currentCol; yc >= 0; yc--)
                {
                    int gridX = gridPos.X + xc * octantTransform.xx + yc * octantTransform.xy;
                    int gridY = gridPos.Y + xc * octantTransform.yx + yc * octantTransform.yy;

                    if (gridX < 0 || gridX >= gridSize.X || gridY < 0 || gridY >= gridSize.Y)
                    {
                        continue;
                    }

                    float leftBlockSlope = (yc + 0.5f) / (xc - 0.5f);
                    float rightBlockSlope = (yc - 0.5f) / (xc + 0.5f);

                    if (rightBlockSlope > leftViewSlope)
                    {
                        continue;
                    }

                    if (leftBlockSlope < rightViewSlope)
                    {
                        break;
                    }

                    float sqrMagnitude = xc * xc + yc * yc;
                    if (sqrMagnitude <= viewRadiusSqr)
                    {
                        visibleTiles.Add(new Vector2(gridX, gridY));
                        //grid.SetLight(gridX, gridY, sqrMagnitude);
                    }

                    bool curBlocked = GameWorld.GetEntitiesAtPosition(new Vector2(gridX, gridY)).Any(e => e.HasComponent(ComponentLookup.BlockMovement)/*(typeof(BlockVisionComponent))*/);//grid.IsWall(gridX, gridY);

                    if (prevWasBlocked)
                    {
                        if (curBlocked)
                        {
                            savedRightSlope = rightBlockSlope;
                        }
                        else
                        {
                            prevWasBlocked = false;
                            leftViewSlope = savedRightSlope;
                        }
                    }
                    else
                    {
                        if (curBlocked)
                        {
                            if (leftBlockSlope <= leftViewSlope)
                            {
                                CastLight(gridSize, gridPos, viewRadius, currentCol + 1,
                                    leftViewSlope, leftBlockSlope, octantTransform, ref visibleTiles);
                            }

                            prevWasBlocked = true;
                            savedRightSlope = rightBlockSlope;
                        }
                    }
                }
                
                if (prevWasBlocked)
                {
                    break;
                }
            }
        }
    }
}