using OpenTK.Graphics;
using Roguelike.Components;
using Roguelike.Helpers;
using SunshineConsole;

namespace Roguelike.UI
{
    // ReSharper disable once InconsistentNaming
    public class HUD
    {
        private readonly Entity _playerEntity;
        private readonly ConsoleWindow _console;
        private readonly int _yOffset;
        
        private const int YStep = 1;

        public HUD(Entity playerEntity, ConsoleWindow console)
        {
            _playerEntity = playerEntity;
            _console = console;
            _yOffset = _console.Rows / 2;
        }
        
        public void Draw()
        {
            var step = YStep;

            var health = "HP: " + _playerEntity.GetComponent<HealthComponent>(ComponentLookup.Health)?.Health;
            _console.Write(_yOffset + step, 0, health, Color4.White);
            step += YStep;

            var armorClass = "AC: " + EquipmentHelper.GetArmorClass(_playerEntity);
            _console.Write(_yOffset + step, 0, armorClass, Color4.White);
            step += YStep;

            var weapon = "W: " + CombatHelper.GetWeapon(_playerEntity)?.RetainedBy.Name;
            _console.Write(_yOffset + step, 0, weapon, Color4.White);
            step += YStep;
        }
    }
}