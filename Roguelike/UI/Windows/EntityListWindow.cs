using System.Collections.Generic;
using OpenTK.Graphics;
using Roguelike.Helpers;
using SunshineConsole;

namespace Roguelike.UI.Windows
{
    public abstract class EntityListWindow : Window
    {
        private readonly Color4 _color = Color4.LightGray;
        private readonly Color4 _cursorColor = Color4.LightYellow;
        private readonly int _positionX;

        private const int PositionY = 1;
        private const int OffsetY = 1;
        private const char CursorSymbol = '>';
        
        public EntityListWindow(string windowName, ConsoleWindow consoleWindow, InputManager inputManager, InputState windowInputState)
            : base(windowName, consoleWindow, inputManager, windowInputState)
        {
            inputManager.MovePressed += InputManagerOnMovePressed;
            inputManager.EnterPressed += InputManagerOnEnterPressed;
            inputManager.DropPressed += InputManagerOnDropPressed;

            _positionX = consoleWindow.Cols / 2;
        }


        protected abstract List<Entity> Entities { get; }

        protected Entity SelectedEntity => _cursorPosition >= Entities.Count ? null : Entities[_cursorPosition];

        private int _cursorPosition;

        protected virtual string GetEntityName(Entity entity)
        {
            return entity.Name;
        }

        protected virtual void InputManagerOnDropPressed()
        {
            
        }

        protected virtual void InputManagerOnEnterPressed()
        {
            
        }

        protected override void DrawWindow()
        {
            var positionX = _positionX - WindowName.Length / 2;
            
            ConsoleWindow.Write(OffsetY -1, positionX, WindowName, _color);
            
            for (var i = 0; i < Entities.Count; i++)
            {
                var itemName = GetEntityName(Entities[i]);
                if (_cursorPosition == i)
                    ConsoleWindow.Write(PositionY + OffsetY * i, positionX - 1, CursorSymbol, _cursorColor);
                
                ConsoleWindow.Write(PositionY + OffsetY * i, positionX, itemName, _color);
            }
        }

        protected override void Show()
        {
            base.Show();
            _cursorPosition = 0;
        }

        protected override void Hide()
        {
            base.Hide();
            ConsoleWindowHelper.ClearConsole(ConsoleWindow);
        }

        private void InputManagerOnDropPressed(InputState inputState)
        {
            if (inputState != WindowInputState) return;
            InputManagerOnDropPressed();
        }

        private void InputManagerOnEnterPressed(InputState inputState)
        {
            if (inputState != WindowInputState) return;
            InputManagerOnEnterPressed();
        }

        private void InputManagerOnMovePressed(Vector2 move, InputState inputState)
        {
            if (inputState != WindowInputState) return;
            
            ConsoleWindowHelper.ClearConsole(ConsoleWindow);
            if (move == Vector2.Up) CursorUp();
            else if (move == Vector2.Down) CursorDown();
        }

        private void CursorUp()
        {
            _cursorPosition--;
            if (_cursorPosition < 0)
                _cursorPosition = Entities.Count - 1;
        }

        private void CursorDown()
        {
            _cursorPosition++;
            if (_cursorPosition > Entities.Count - 1)
                _cursorPosition = 0;
        }
    }
}