using System.Collections.Generic;
using System.Linq;
using Roguelike.Components;
using Roguelike.Helpers;
using SunshineConsole;

namespace Roguelike.UI.Windows
{
    public class EquipmentWindow : EntityListWindow
    {
        private readonly Entity _playerEntity;

        public EquipmentWindow(Entity playerEntity, string windowName, ConsoleWindow consoleWindow, InputManager inputManager, InputState windowInputState)
            : base(windowName, consoleWindow, inputManager, windowInputState)
        {
            _playerEntity = playerEntity;
            inputManager.EquipmentPressed += Toggle;
        }

        protected override void InputManagerOnEnterPressed()
        {
            if (SelectedEntity == null) return;

            var equipment = SelectedEntity.GetComponent<EquipmentComponent>(ComponentLookup.Equipment);
            if (equipment == null) return;
            if (equipment.SlotType == EquipmentSlotType.Weapon)
                _playerEntity.AddComponent(new ActionComponent(ActionType.UnequipWeapon, SelectedEntity));
            else// if (SelectedEntity.HasComponent(typeof(ArmorComponent)))
                _playerEntity.AddComponent(new ActionComponent(ActionType.UnequipArmor, SelectedEntity));
            
            ConsoleWindowHelper.ClearConsole(ConsoleWindow);
        }

        protected override string GetEntityName(Entity entity)
        {
            var name = base.GetEntityName(entity);
            return $"{name} - {entity.GetComponent<EquipmentComponent>(ComponentLookup.Equipment).SlotType}";
        }

        protected override List<Entity> Entities
        {
            get
            {
                var equipmentComponent = _playerEntity.GetComponent<EquipmentSlotsComponent>(ComponentLookup.EquipmentSlot);
                var equipmentList = equipmentComponent.EquipmentSlots.Where(a => a.Equipment != null).Select(a => a.Equipment.RetainedBy).ToList();
                return equipmentList;
            }
        }
    }
}