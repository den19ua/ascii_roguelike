using System.Collections.Generic;
using System.Linq;
using Roguelike.Components;
using Roguelike.Helpers;
using SunshineConsole;

namespace Roguelike.UI.Windows
{
    public class InventoryWindow : EntityListWindow
    {
        private readonly Entity _playerEntity;

        public InventoryWindow(Entity playerEntity, string windowName, ConsoleWindow consoleWindow, InputManager inputManager, InputState windowInputState)
            : base(windowName, consoleWindow, inputManager, windowInputState)
        {
            _playerEntity = playerEntity;
            inputManager.InventoryPressed += Toggle;
        }

        protected override void InputManagerOnEnterPressed()
        {
            if (SelectedEntity == null) return;

            var equipment = SelectedEntity.GetComponent<EquipmentComponent>(ComponentLookup.Equipment);
            if (equipment == null) return;
            if (equipment.SlotType == EquipmentSlotType.Weapon)//(SelectedEntity.HasComponent(typeof(WeaponComponent)))
                _playerEntity.AddComponent(new ActionComponent(ActionType.EquipWeapon, SelectedEntity));
            else// if (SelectedEntity.HasComponent(typeof(ArmorComponent)))
                _playerEntity.AddComponent(new ActionComponent(ActionType.EquipArmor, SelectedEntity));
            
            ConsoleWindowHelper.ClearConsole(ConsoleWindow);
        }

        protected override List<Entity> Entities
        {
            get { return _playerEntity.GetComponent<InventoryComponent>(ComponentLookup.Inventory).Items.Select(i => i.RetainedBy).ToList(); }
        }

        protected override void InputManagerOnDropPressed()
        {            
            if (SelectedEntity == null) return;

            ItemHelper.DropItem(SelectedEntity, _playerEntity);
            ConsoleWindowHelper.ClearConsole(ConsoleWindow);
        }
    }
}