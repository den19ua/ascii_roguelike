using SunshineConsole;

namespace Roguelike.UI.Windows
{
    public abstract class Window
    {
        private readonly InputManager _inputManager;

        public Window(string windowName, ConsoleWindow consoleWindow, InputManager inputManager, InputState windowInputState)
        {
            WindowName = windowName;
            ConsoleWindow = consoleWindow;
            _inputManager = inputManager;
            WindowInputState = windowInputState;
        }
        
        public void Toggle()
        {
            if (!_isOpen) Show();
            else Hide();
        }

        public void Draw()
        {
            if (!_isOpen) return;
            DrawWindow();
        }
        
        protected abstract void DrawWindow();

        protected ConsoleWindow ConsoleWindow { get; }
        protected string WindowName { get; }
        protected InputState WindowInputState { get; }
        
        private bool _isOpen;

        protected virtual void Show()
        {
            if (_inputManager.InputState != InputState.Game) return;
            
            _inputManager.InputState = WindowInputState;
            _isOpen = true;
        }

        protected virtual void Hide()
        {
            if (_inputManager.InputState != WindowInputState) return;
            
            _inputManager.InputState = InputState.Game;
            _isOpen = false;
        }
    }
}