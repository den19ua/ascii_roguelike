namespace Roguelike
{
    public struct Vector2
    {
        public static Vector2 Up => new Vector2(0, -1);
        public static Vector2 Down => new Vector2(0, 1);
        public static Vector2 Left => new Vector2(-1, 0);
        public static Vector2 Right => new Vector2(1, 0);
        public static Vector2 Zero => new Vector2(0, 0);
        
        public int X { get; }
        public int Y { get; }

        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

#region Operators
        public static Vector2 operator+(Vector2 v1, Vector2 v2)
        {
            return new Vector2(
                v1.X + v2.X,
                v1.Y + v2.Y);
        }

        public static bool operator==(Vector2 v1, Vector2 v2)
        {
            return
                v1.X == v2.X &&
                v1.Y == v2.Y;
        }

        public static bool operator !=(Vector2 v1, Vector2 v2)
        {
            return !(v1 == v2);
        }
        
        public bool Equals(Vector2 other)
        {
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Vector2 other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        public override string ToString()
        {
            return $"[{X}, {Y}]";
        }

#endregion
    }
}